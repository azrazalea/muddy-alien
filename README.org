* Muddy Alien

A common lisp mud client designed to work with [[http://www.aetolia.com][Aetolia]], an Iron Realms MUD.

* Planned features
** Terminal only (likely using ncurses wrapper)
** Linux + SBCL support priority. Patches for other OS/implementations welcome.
** Full feature support including GMCP, Alias, Triggers, queue.
** Configurable UI similar to Project Source (see picture of Project Source here) [[http://i.imgur.com/Kyiusx4.jpg][Project Source]]
** Ephemeral and permanent settings support
** Easy system support with DSL
** Everything trackable in git.
