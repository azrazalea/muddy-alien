;;;; muddy-alien.asd

(asdf:defsystem #:muddy-alien
  :description "A common lisp mud client."
  :author "Lily Carpenter <lily-license@azrazalea.net>"
  :license "AGPLv3+"
  :depends-on ("split-sequence"
               "alexandria"
               "cl-ppcre"
               "slynk"
               "telnetlib"
               "iterate"
               "bordeaux-threads"
               "lparallel")
  :serial t
  :pathname "src"
  :components ((:file "package")
               (:file "internal")))
