;;;; package.lisp

(defpackage #:muddy-alien
  (:use)
  (:export #:start))

(defpackage #:muddy-alien-internal
  (:use #:cl
        #:split-sequence
        #:iterate
        #:muddy-alien))
