;;;; muddy-alien-test.asd

(asdf:defsystem #:muddy-alien-test
  :author "Lily Carpenter <lily-license@azrazalea.net>"
  :license "AGPLv3+"
  :depends-on (:muddy-alien
               :prove
               :check-it)
  :pathname "src/test"
  :components ((:file "suite"))
  :description "Testing muddy alien")
